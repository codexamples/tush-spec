<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->unsignedTinyInteger('answers');
            $table->unsignedTinyInteger('score');
            $table->dateTime('finished_at')->nullable();
            $table->string('examinee_uid');
            $table->timestamps();

            $table->foreign('examinee_uid')->references('uid')->on('examinees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
