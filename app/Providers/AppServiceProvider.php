<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Business\Support\Navigation\Breadcrumb::class,
            function () {
                return \App\Support\Navigation\Breadcrumb::start(route('admin'), config('app.name', 'Laravel'));
            }
        );

        $this->app->bind(
            \App\Business\Support\Navigation\CurrentMenu::class,
            function () {
                return \App\Support\Navigation\CurrentMenu::start()->addLeft('index');
            }
        );

        $this->app->bind(
            \App\Business\Support\FastValidator::class,
            \App\Support\FastValidator::class
        );

        $this->app->bind(
            \App\Business\Data\Examining\Examinator::class,
            \App\Models\Examining\Examinator::class
        );

        $this->app->bind(
            \App\Business\Support\TrueTranslator::class,
            \App\Support\TrueTranslator::class
        );

        $this->app->bind(
            \App\Business\Data\Examining\ExamineResultAccessible::class,
            \App\Models\Session::class
        );

        $this->app->bind(
            \App\Business\Data\Examining\ExamineExporter::class,
            \App\Models\Examining\ExamineExporter::class
        );
    }
}
