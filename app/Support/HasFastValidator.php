<?php

namespace App\Support;

use App\Business\Support\FastValidator as FastValidatorContract;

/**
 * Класс, имеющий быстрый валидатор, как синглтон
 */
trait HasFastValidator
{
    /**
     * Быстрый валидатор
     *
     * @var \App\Business\Support\FastValidator
     */
    private $fastValidator;

    /**
     * Возвращает быстрый валидатор
     *
     * @return \App\Business\Support\FastValidator
     */
    protected function fastValidator()
    {
        if ($this->fastValidator === null) {
            $this->fastValidator = app()->make(FastValidatorContract::class);
        }

        return $this->fastValidator;
    }
}
