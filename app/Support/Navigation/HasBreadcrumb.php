<?php

namespace App\Support\Navigation;

use App\Business\Support\Navigation\Breadcrumb;

/**
 * Класс, имеющий хлебные крошки, как синглтон
 */
trait HasBreadcrumb
{
    /**
     * Хлебные крошки
     *
     * @var \App\Business\Support\Navigation\Breadcrumb
     */
    private $breadcrumb;

    /**
     * Возвращает самую первую хлебную крошку
     *
     * @return \App\Business\Support\Navigation\Breadcrumb
     */
    protected function breadcrumb()
    {
        if ($this->breadcrumb === null) {
            $this->breadcrumb = app()->make(Breadcrumb::class);
        }

        return $this->breadcrumb;
    }
}
