<?php

namespace App\Support;

use App\Business\Support\FastValidator as FastValidatorContract;
use Illuminate\Support\Facades\Validator;

/**
 * Реализация быстрого валидатора
 */
class FastValidator implements FastValidatorContract
{
    /**
     * Создает новый валидатор
     *
     * @param  array  $data
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customs
     * @return \Illuminate\Validation\Validator
     */
    public function makeValidator(array $data = [], array $rules = [], array $messages = [], array $customs = [])
    {
        return Validator::make($data, $rules, $messages, $customs);
    }

    /**
     * Создает валидатор с ошибками
     *
     * @param  array $messages
     * @return \Illuminate\Validation\Validator
     */
    public function makeErroneousValidator(array $messages)
    {
        $validator = $this->makeValidator();

        foreach ($messages as $key => $msg) {
            $validator->messages()->add($key, $msg);
        }

        return $validator;
    }
}
