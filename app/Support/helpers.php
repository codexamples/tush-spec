<?php

if (!function_exists('word_end_choice')) {
    /**
     * Возвращает один из вариантов окончания слова, в зависимости от числа
     *
     * @param  float  $n
     * @param  string  $sa
     * @param  string  $sb
     * @param  string  $sc
     * @return string
     */
    function word_end_choice(float $n, string $sa, string $sb, string $sc)
    {
        $n = floor(abs($n));
        $m = $n % 100;

        if ($m > 10 && $m < 15) {
            return $sc;
        }

        $m = $n % 10;

        if ($m === 1) {
            return $sa;
        }

        if ($m > 1 && $m < 5) {
            return $sb;
        }

        return $sc;
    }
}
