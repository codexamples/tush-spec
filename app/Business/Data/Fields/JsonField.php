<?php

namespace App\Business\Data\Fields;

/**
 * Доступ к JSON-полям
 */
trait JsonField
{
    /**
     * Возвращает массив из JSON-поля
     *
     * @param  string  $field
     * @return array
     */
    public function getJsonValue(string $field)
    {
        $arr = json_decode($this->getFieldValue($field), true);

        return is_array($arr) ? $arr : [];
    }

    /**
     * Задает JSON-поле из массива
     *
     * @param  string  $field
     * @param  array  $value
     * @return void
     */
    public function setJsonValue(string $field, array $value)
    {
        $this->setFieldValue($field, json_encode($value, JSON_UNESCAPED_UNICODE));
    }
}
