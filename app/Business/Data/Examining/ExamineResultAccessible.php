<?php

namespace App\Business\Data\Examining;

/**
 * Доступ к результату тестирования
 */
interface ExamineResultAccessible
{
    /**
     * Возвращает результат тестирования
     *
     * @return int
     */
    public function getExamineResult();

    /**
     * Генерирует картинку с результатом тестирования
     *
     * @param  string  $target
     * @return void
     */
    public function generateResultImage(string $target);

    /**
     * Возвращает тексты результата тестирования
     *
     * @return array
     */
    public function getExamineResultTexts();
}
