<?php

namespace App\Business\Data\Examining;

use Exception;

/**
 * Экспортер результатов тестирования
 */
interface ExamineExporter
{
    /**
     * Генерирует и возвращает имя файла для выгрузки
     *
     * @return string
     */
    public function generateStatsHash();

    /**
     * Возвращает true, если выгрузка готова
     *
     * @param  string  $hash
     * @return bool
     */
    public function areStatsReady(string $hash);

    /**
     * Возвращает ссылку на скачивание файла выгрузки
     *
     * @param  string  $hash
     * @return string
     */
    public function getStatsUrl(string $hash);

    /**
     * Экспортирует статистику по тестированиям
     *
     * @param  string  $hash
     * @return void
     */
    public function exportStats(string $hash);

    /**
     * Оповещает об ошибке во время экспорта
     *
     * @param  string  $hash
     * @param  \Exception  $ex
     * @return void
     */
    public function erroneousStats(string $hash, Exception $ex);
}
