<?php

namespace App\Business\Data\Examining;

use Illuminate\Http\Request;

/**
 * Фукнционал прохождения тестирования
 */
interface Examinator
{
    /**
     * Отвечает на анкетный вопрос
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function answerPersonal(Request $request);

    /**
     * Отвечает на вопрос теста
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function answerQuestion(Request $request);

    /**
     * Завершает тестирование
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function finishExamine(Request $request);

    /**
     * Отвечает на вопрос в строковом виде
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function indexedAnswer(Request $request);
}
