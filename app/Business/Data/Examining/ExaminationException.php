<?php

namespace App\Business\Data\Examining;

use Exception;

/**
 * Ошибка во время прохождения тестирования
 */
class ExaminationException extends Exception
{
}
