<?php

namespace App\Business\Support;

/**
 * Быстрый валидатор
 */
interface FastValidator
{
    /**
     * Создает новый валидатор
     *
     * @param  array  $data
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customs
     * @return \Illuminate\Validation\Validator
     */
    public function makeValidator(array $data = [], array $rules = [], array $messages = [], array $customs = []);

    /**
     * Создает валидатор с ошибками
     *
     * @param  array $messages
     * @return \Illuminate\Validation\Validator
     */
    public function makeErroneousValidator(array $messages);
}
