<?php

namespace App\Business\Support;

/**
 * Правильный переводчик
 */
interface TrueTranslator
{
    /**
     * Возвращает true, если перевод имеется в наличии
     *
     * @param  string  $key
     * @return bool
     */
    public function hasTranslation(string $key);

    /**
     * Возвращает перевод по указанному ключу, возвращая последний кусок ключа, если перевода нет
     *
     * @param  string  $key
     * @param  array  $replace
     * @param  $locale
     * @return string
     */
    public function translate(string $key, array $replace = [], $locale = null);

    /**
     * Возвращает перевод на основе числа, возвращая последний кусок ключа, если перевода нет
     *
     * @param  string  $key
     * @param  $number
     * @param  array  $replace
     * @param  $locale
     * @return string
     */
    public function translateChoice(string $key, $number, array $replace = [], $locale = null);

    /**
     * Возвращает перевод по указанному ключу, возвращая последний кусок ключа, если перевода нет
     *
     * @param  string  $key
     * @param  array  $replace
     * @param  $locale
     * @return string
     */
    public function translateJson(string $key, array $replace = [], $locale = null);
}
