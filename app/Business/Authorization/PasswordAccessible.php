<?php

namespace App\Business\Authorization;

/**
 * Функционал по доступу и смене пароля
 */
interface PasswordAccessible
{
    /**
     * Возвращает true, если указанный пароль совпадает с текущим
     *
     * @param  string  $password
     * @return bool
     */
    public function passwordAsserts(string $password);

    /**
     * Сменяет пароль
     *
     * @param  string  $new_password
     * @return void
     */
    public function changePassword(string $new_password);
}
