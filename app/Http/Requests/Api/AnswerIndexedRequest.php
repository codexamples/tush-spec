<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class AnswerIndexedRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uid' => 'required|max:127',
            'string' => 'required|max:1023',
            'ga' => 'nullable|max:64',
        ];
    }
}
