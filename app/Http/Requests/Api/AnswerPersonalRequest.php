<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class AnswerPersonalRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uid' => 'required|max:127',
            'field' => 'required|max:32',
            'value' => 'required|max:32',
        ];
    }
}
