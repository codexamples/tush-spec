<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class AnswerQuestionRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uid' => 'required|max:127',
            'index' => 'required|integer|between:0,255',
            'value' => 'required|integer|between:0,255',
        ];
    }
}
