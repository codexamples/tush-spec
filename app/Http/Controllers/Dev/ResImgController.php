<?php

namespace App\Http\Controllers\Dev;

use App\Business\Data\Examining\ExamineResultAccessible;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ResImgController extends Controller
{
    /**
     * Проверка генерации результирующего изображения
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /** @var  \App\Business\Data\Examining\ExamineResultAccessible|\Illuminate\Database\Eloquent\Model  $session */
        $session = app()->make(ExamineResultAccessible::class);

        $session->fill($request->all());
        $session->id = 'dev';

        foreach (config('examining.targets') as $target) {
            $session->generateResultImage($target);
        }

        return view('routes.dev.res_img', [
            'session' => $session,
        ]);
    }
}
