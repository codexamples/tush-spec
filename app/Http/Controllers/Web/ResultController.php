<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Repositories\SessionRepository;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Страница результата тестирования
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $session_id
     * @param  \App\Models\Repositories\SessionRepository  $sessions
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, int $session_id, SessionRepository $sessions)
    {
        $session = $sessions->one($session_id);

        if ($session->finished_at) {
            return view('routes.web.result', [
                'session' => $session,
                'target' => $request->input('target'),
            ]);
        }

        return response()->make('This session is not finished yet', 404);
    }
}
