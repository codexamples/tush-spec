<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Repositories\ExamineeRepository;

class IndexController extends Controller
{
    /**
     * Главная страница a.k.a. React-Based
     *
     * @param  \App\Models\Repositories\ExamineeRepository  $examinees
     * @return \Illuminate\Http\Response
     */
    public function index(ExamineeRepository $examinees)
    {
        $css = public_path('css/index.css');
        $js = public_path('bundle.js');

        return view('routes.web.index', [
            'uid' => $examinees->determine()->uid,
            'hash' => [
                'css' => file_exists($css) ? md5_file($css) : 'style',
                'js' => file_exists($js) ? md5_file($js) : 'script'
            ],
        ]);
    }
}
