<?php

namespace App\Http\Controllers\Api;

use App\Business\Data\Examining\Examinator;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AnswerFinishRequest;
use App\Http\Requests\Api\AnswerIndexedRequest;
use App\Http\Requests\Api\AnswerPersonalRequest;
use App\Http\Requests\Api\AnswerQuestionRequest;

class AnswerController extends Controller
{
    /**
     * Функционал прохождения тестирования
     *
     * @var \App\Business\Data\Examining\Examinator
     */
    private $examinator;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Business\Data\Examining\Examinator  $examinator
     */
    public function __construct(Examinator $examinator)
    {
        $this->examinator = $examinator;
    }

    /**
     * Возвращает функционал прохождения тестирования
     *
     * @return \App\Business\Data\Examining\Examinator
     */
    protected function examinator()
    {
        return $this->examinator;
    }

    /**
     * Ответ на анкетный вопрос
     *
     * @param  \App\Http\Requests\Api\AnswerPersonalRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function personal(AnswerPersonalRequest $request)
    {
        return response()->json(
            $this->examinator()->answerPersonal($request)
        );
    }

    /**
     * Ответ на вопрос теста
     *
     * @param  \App\Http\Requests\Api\AnswerQuestionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function question(AnswerQuestionRequest $request)
    {
        return response()->json(
            $this->examinator()->answerQuestion($request)
        );
    }

    /**
     * Завершение теста
     *
     * @param  \App\Http\Requests\Api\AnswerFinishRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function finish(AnswerFinishRequest $request)
    {
        return response()->json(
            $this->examinator()->finishExamine($request)
        );
    }

    /**
     * Строковой ответ на вопрос
     *
     * @param  \App\Http\Requests\Api\AnswerIndexedRequest  $request
     * @param  int  $question_index
     * @return \Illuminate\Http\Response
     */
    public function indexed(AnswerIndexedRequest $request, int $question_index)
    {
        $request->merge(['question_index' => $question_index]);

        return response()->json(
            $this->examinator()->indexedAnswer($request)
        );
    }
}
