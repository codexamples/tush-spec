<?php

namespace App\Http\Controllers\Api;

use App\Business\Data\Examining\ExamineExporter;
use App\Http\Controllers\Controller;
use App\Jobs\ExportStats;

class StatsExportController extends Controller
{
    /**
     * Экспортер результатов тестирования
     *
     * @var \App\Business\Data\Examining\ExamineExporter
     */
    private $exporter;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Business\Data\Examining\ExamineExporter  $exporter
     */
    public function __construct(ExamineExporter $exporter)
    {
        $this->exporter = $exporter;
    }

    /**
     * Возвращает экспортера результатов тестирования
     *
     * @return \App\Business\Data\Examining\ExamineExporter
     */
    protected function exporter()
    {
        return $this->exporter;
    }

    /**
     * Начало выгрузки всей статистики
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {
        $hash = $this->exporter()->generateStatsHash();

        dispatch(new ExportStats($hash));
        return response()->make($hash);
    }

    /**
     * Проверка стадии выгрузки
     *
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function status(string $hash)
    {
        return response()->make(
            $this->exporter()->areStatsReady($hash) ? $this->exporter()->getStatsUrl($hash) : ''
        );
    }
}
