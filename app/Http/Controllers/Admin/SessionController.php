<?php

namespace App\Http\Controllers\Admin;

use App\Models\Repositories\ExamineeRepository;
use App\Models\Repositories\SessionRepository;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    /**
     * Репозиторий сессий
     *
     * @var \App\Models\Repositories\SessionRepository
     */
    private $sessions;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\SessionRepository  $sessions
     */
    public function __construct(SessionRepository $sessions)
    {
        $this->breadcrumb()->append(route('admin.session'), 'Сессии');
        $this->currentMenu()->addLeft('session');

        $this->sessions = $sessions;
    }

    /**
     * Возвращает репозиторий сессий
     *
     * @return \App\Models\Repositories\SessionRepository
     */
    protected function sessions()
    {
        return $this->sessions;
    }

    /**
     * Список сессий
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sessions = $this->sessions()->all($request);

        if ($request->has('uid')) {
            /** @var  \App\Models\Repositories\ExamineeRepository  $examinees */
            $examinees = app()->make(ExamineeRepository::class);

            $examinee = $examinees->one($request->input('uid'));
            $this->breadcrumb()->append(
                route('admin.session', ['uid' => $examinee->uid]),
                'Сес.Исп: ' . $examinee->short_uid
            );

            $sessions->appends('uid', $examinee->uid);
        }

        return view('routes.admin.session.index', $this->fillViewData([
            'sessions' => $sessions,
        ]));
    }

    /**
     * Просмотр сессии
     *
     * @param  string  $uid
     * @return \Illuminate\Http\Response
     */
    public function view(string $uid)
    {
        $session = $this->sessions()->one($uid);
        $this->breadcrumb()->append(route('admin.session.view', $session->id), '№' . $session->id);

        return view('routes.admin.session.view', $this->fillViewData([
            'session' => $session,
        ]));
    }
}
