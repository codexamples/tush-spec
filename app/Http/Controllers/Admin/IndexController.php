<?php

namespace App\Http\Controllers\Admin;

class IndexController extends Controller
{
    /**
     * Главная страница админки
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('routes.admin.index', $this->fillViewData());
    }
}
