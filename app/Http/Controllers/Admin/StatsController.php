<?php

namespace App\Http\Controllers\Admin;

use App\Models\Repositories\ExamineeRepository;
use App\Models\Repositories\SessionRepository;

class StatsController extends Controller
{
    /**
     * Некоторая статистика по тестированиям
     *
     * @param  \App\Models\Repositories\SessionRepository  $sessions
     * @param  \App\Models\Repositories\ExamineeRepository  $examinees
     * @return \Illuminate\Http\Response
     */
    public function index(SessionRepository $sessions, ExamineeRepository $examinees)
    {
        $this->breadcrumb()->append(route('admin.stats'), 'Статистика');
        $this->currentMenu()->addLeft('stats');

        return view('routes.admin.stats', $this->fillViewData([
            'stats' => array_merge($sessions->stats(), $examinees->stats()),
        ]));
    }
}
