<?php

namespace App\Http\Controllers\Admin;

use App\Models\Repositories\ExamineeRepository;

class ExamineeController extends Controller
{
    /**
     * Репозиторий испытуемых
     *
     * @var \App\Models\Repositories\ExamineeRepository
     */
    private $examinees;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\ExamineeRepository  $examinees
     */
    public function __construct(ExamineeRepository $examinees)
    {
        $this->breadcrumb()->append(route('admin.examinee'), 'Испытуемые');
        $this->currentMenu()->addLeft('examinee');

        $this->examinees = $examinees;
    }

    /**
     * Возвращает репозиторий испытуемых
     *
     * @return \App\Models\Repositories\ExamineeRepository
     */
    protected function examinees()
    {
        return $this->examinees;
    }

    /**
     * Список испытуемых
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('routes.admin.examinee.index', $this->fillViewData([
            'examinees' => $this->examinees()->all(),
            'cookie_uid' => $this->examinees()->cookieUid(),
        ]));
    }

    /**
     * Просмотр испытуемого
     *
     * @param  string  $uid
     * @return \Illuminate\Http\Response
     */
    public function view(string $uid)
    {
        $examinee = $this->examinees()->one($uid);
        $this->breadcrumb()->append(route('admin.examinee.view', $examinee->uid), $examinee->short_uid);

        return view('routes.admin.examinee.view', $this->fillViewData([
            'examinee' => $examinee,
            'cookie_uid' => $this->examinees()->cookieUid(),
        ]));
    }
}
