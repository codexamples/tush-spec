<?php

namespace App\Models;

use App\Business\Authorization\AdminAccessible;
use App\Business\Authorization\PasswordAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements AdminAccessible, PasswordAccessible
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Возвращает true, если пользователь является админом
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->id === 1;
    }

    /**
     * Возвращает true, если указанный пароль совпадает с текущим
     *
     * @param  string  $password
     * @return bool
     */
    public function passwordAsserts(string $password)
    {
        return Hash::check($password, $this->password);
    }

    /**
     * Сменяет пароль
     *
     * @param  string  $new_password
     * @return void
     */
    public function changePassword(string $new_password)
    {
        $this->password = Hash::make($new_password);
        $this->save();
    }
}
