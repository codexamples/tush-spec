<?php

namespace App\Models;

use App\Business\Data\Examining\ExamineResultAccessible;
use App\Business\Data\Fields\FieldAccess;
use App\Business\Data\Fields\FileField;
use App\Support\HasTrueTranslator;
use Illuminate\Database\Eloquent\Model;
use Imagick;
use ImagickDraw;
use ImagickPixel;

class Session extends Model implements ExamineResultAccessible
{
    use FieldAccess, FileField, HasTrueTranslator;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sex',
        'age',
        'answers',
        'score',
        'finished_at',
        'examinee_uid',
    ];

    /**
     * Examinee - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examinee()
    {
        return $this->belongsTo(Examinee::class, 'examinee_uid');
    }

    /**
     * Answer - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answerModels()
    {
        return $this->hasMany(Answer::class, 'session_id');
    }

    /**
     * Возвращает пол в удобочитаемом виде
     *
     * @return string
     */
    public function getHumanSexAttribute()
    {
        $s = $this->getFieldValue('sex');
        $arr = [
            'm' => 'Мужской',
            'f' => 'Женский',
        ];

        return array_key_exists($s, $arr) ? $arr[$s] : 'Неизвестен';
    }

    /**
     * Возвращает возраст в удобочитаемом виде
     *
     * @return string
     */
    public function getHumanAgeAttribute()
    {
        $s = $this->getFieldValue('age');
        $arr = [];

        if (starts_with($s, '_')) {
            $arr[] = '<';
            $arr[] = mb_substr($s, 1);
        } elseif (ends_with($s, '_')) {
            $arr[] = '>';
            $arr[] = mb_substr($s, 0, -1);
        } elseif (mb_strpos($s, '-') !== false) {
            $arr[] = str_replace('-', ' - ', $s);
        } else {
            $arr[] = empty($s) ? 'Неизвестно' : $s;
        }

        return implode(' ', $arr);
    }

    /**
     * Возвращает название для картинки с результатом тестирования
     *
     * @param  string  $suffix
     * @return string
     */
    protected function getResultImageName(string $suffix)
    {
        return $this->id . $suffix . '.png';
    }

    /**
     * Возвращает ссылку на картинку с результатом тестирования
     *
     * @return string
     */
    public function getResultImageAttribute()
    {
        return $this->getFileStorage()->url(
            $this->getResultImageName('')
        );
    }

    /**
     * Возвращает ссылку на картинку с результатом тестирования для соц.сети "одноклассники"
     *
     * @return string
     */
    public function getResultImageOkAttribute()
    {
        return $this->getFileStorage()->url(
            $this->getResultImageName('_ok')
        );
    }

    /**
     * Возвращает результат тестирования
     *
     * @return int
     */
    public function getExamineResult()
    {
        return $this->score;
    }

    /**
     * Генерирует картинку с результатом тестирования
     *
     * @param  string  $target
     * @return void
     */
    public function generateResultImage(string $target)
    {
        $suffix = empty($target) ? '' : '_' . $target;
        $path = $this->getFileStorage()->path(
            $this->getResultImageName($suffix)
        );

        $result = $this->getExamineResult();
        $source = public_path('assets/result_' . $this->sex . $suffix . '.png');

        if (!file_exists($source)) {
            $source = public_path('assets/result_m' . $suffix . '.png');
        }

        $img = new Imagick($source);
        $drw = new ImagickDraw();
        $pxl = new ImagickPixel('white');

        $x = 120.0;
        $y = 105.0;

        $drw->setFont(base_path('resources/assets/fonts/MyriadPro-Regular.ttf'));
        $drw->setFontSize(56);
        $drw->setStrokeColor($pxl);
        $drw->setFillColor($pxl);
        $img->annotateImage($drw, $x, $y, 0, $result);

        $size = $img->queryFontMetrics($drw, $result);
        $x += 2.5;
        $y += ($size['textHeight'] / 4.0);

        $drw = clone $drw;
        $drw->setFontSize(16);
        $img->annotateImage($drw, $x, $y, 0, ' час' . word_end_choice($result, '', 'а', 'ов'));

        $img->writeImage($path);
        chmod($path, 0777);
    }

    /**
     * Возвращает тексты результата тестирования
     *
     * @return array
     */
    public function getExamineResultTexts()
    {
        // text A

        $arr = [
            [0, 4],
            [5, 8],
            [9, 12],
            [13, 16],
            [17, 20],
            [21, 24],
        ];

        $a = [];

        foreach ($arr as $item) {
            for ($i = $item[0]; $i <= $item[1]; $i++) {
                $key = 'sessions.results.a.' . $item[0] . '-' . $item[1];

                if ($this->trueTranslator()->hasTranslation($key)) {
                    $a[$i] = $this->trueTranslator()->translate($key);
                }
            }
        }

        // result

        $result = $this->getExamineResult();
        $arr = [];

        if (array_key_exists($result, $a)) {
            $a = explode('|', $a[$result]);

            $arr['a'] = count($a) > 1 ? [
                'title' => $a[0],
                'text' => $a[1],
            ] : [
                'title' => '',
                'text' => $a[0],
            ];
        } else {
            $arr['a'] = [
                'title' => '',
                'text' => '',
            ];
        }

        $b = 'sessions.results.b.' . $this->sex . '.' . $this->age;
        $arr['b'] = $this->trueTranslator()->hasTranslation($b) ? $this->trueTranslator()->translate($b) : '';

        return $arr;
    }
}
