<?php

namespace App\Models;

use App\Business\Data\Fields\FieldAccess;
use App\Business\Data\Fields\JsonField;
use Illuminate\Database\Eloquent\Model;

class Examinee extends Model
{
    use FieldAccess, JsonField;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'data',
        'ga',
    ];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Session - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(Session::class, 'examinee_uid');
    }

    /**
     * Возвращает короткую версию уникального идентификатора
     *
     * @return string
     */
    public function getShortUidAttribute()
    {
        $s = $this->getFieldValue('uid');

        return mb_substr($s, 0, min(8, mb_strlen($s)));
    }

    /**
     * Возвращает данные в виде JSON-а
     *
     * @return array
     */
    public function getDataAttribute()
    {
        return $this->getJsonValue('data');
    }

    /**
     * Задает данные, как JSON
     *
     * @param  array  $value
     * @return void
     */
    public function setDataAttribute(array $value)
    {
        $this->setJsonValue('data', $value);
    }

    /**
     * Преобразует определенное поле из набора данных
     *
     * @param  string  &$field
     * @param  &$value
     */
    public function mutateDataField(string &$field, &$value)
    {
        // functions

        $stamp_v = function ($value) {
            return date('Y-m-d H:i:s', $value);
        };

        // config

        $arr = [

            'ip' => [
                'k' => 'IP-адрес',
                'v' => null,
            ],

            'stamp' => [
                'k' => 'Дата/Время',
                'v' => $stamp_v,
            ],

        ];

        // mutation

        if (array_key_exists($field, $arr)) {
            $info = $arr[$field];

            $field = $info['k'];
            $value = is_callable($info['v']) ? call_user_func($info['v'], $value) : $value;
        }
    }
}
