<?php

namespace App\Models\Examining;

use App\Business\Data\Examining\ExaminationException;
use App\Business\Data\Examining\Examinator as ExaminatorContract;
use App\Models\Repositories\AnswerRepository;
use App\Models\Repositories\ExamineeRepository;
use App\Models\Repositories\SessionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Реализация фукнционала прохождения тестирования
 */
class Examinator implements ExaminatorContract
{
    /**
     * Репозиторий испытуемых
     *
     * @var \App\Models\Repositories\ExamineeRepository
     */
    protected $examinees;

    /**
     * Репозиторий сессий
     *
     * @var \App\Models\Repositories\SessionRepository
     */
    protected $sessions;

    /**
     * Репозиторий ответов
     *
     * @var \App\Models\Repositories\AnswerRepository
     */
    protected $answers;

    /**
     * Создает экземпляр
     *
     * @param  \App\Models\Repositories\ExamineeRepository  $examinees
     * @param  \App\Models\Repositories\SessionRepository  $sessions
     * @param  \App\Models\Repositories\AnswerRepository  $answers
     */
    public function __construct(ExamineeRepository $examinees, SessionRepository $sessions, AnswerRepository $answers)
    {
        $this->examinees = $examinees;
        $this->sessions = $sessions;
        $this->answers = $answers;
    }

    /**
     * Отвечает на анкетный вопрос
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function answerPersonal(Request $request)
    {
        $examinee = $this->examinees->one($request->input('uid'));
        $session = $this->sessions->last($examinee->uid);
        $field = $request->input('field');
        $value = $request->input('value');

        if (!$session || $session->finished_at || $session->$field) {
            $request = new Request();
            $request->merge([
                'sex' => null,
                'age' => null,
                'answers' => 0,
                'score' => 0,
                'finished_at' => null,
                'examinee_uid' => $examinee->uid,
            ]);

            $session = $this->sessions->create($request);
        }

        $session->$field = $value;
        $session->save();

        return [
            'ok' => true,
            'id' => $session->id,
        ];
    }

    /**
     * Отвечает на вопрос теста
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     * @throws \App\Business\Data\Examining\ExaminationException
     */
    public function answerQuestion(Request $request)
    {
        $examinee = $this->examinees->one($request->input('uid'));
        $session = $this->sessions->last($examinee->uid);
        $index = intval($request->input('index'));
        $value = intval($request->input('value'));

        if (!$session) {
            throw new ExaminationException('No sessions found in ' . $examinee->uid);
        }

        if ($session->finished_at) {
            throw new ExaminationException('The last session is already finished');
        }

        if ($session->answers !== $index) {
            throw new ExaminationException(
                'Invalid question index (' . $index . ') in session with (' . $session->answers . ') answers'
            );
        }

        $session->answers++;
        $session->score += $value;
        $session->save();

        return [
            'ok' => true,
            'id' => $session->id,
        ];
    }

    /**
     * Завершает тестирование
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     * @throws \App\Business\Data\Examining\ExaminationException
     */
    public function finishExamine(Request $request)
    {
        $examinee = $this->examinees->one($request->input('uid'));
        $session = $this->sessions->last($examinee->uid);

        if (!$session) {
            throw new ExaminationException('No sessions found in ' . $examinee->uid);
        }

        if ($session->finished_at) {
            throw new ExaminationException('The last session is already finished');
        }

        foreach (config('examining.targets') as $target) {
            $session->generateResultImage($target);
        }

        $session->finished_at = new Carbon();
        $session->save();

        return [
            'ok' => true,
            'id' => $session->id,
            'page' => route('result', $session->id),
            'page_ok' => route('result', [
                $session->id,
                'target' => 'ok',
            ]),
            'image' => $session->result_image,
            'image_ok' => $session->result_image_ok,
            'result' => $session->getExamineResult(),
            'texts' => $session->getExamineResultTexts(),
        ];
    }

    /**
     * Отвечает на вопрос в строковом виде
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     * @throws \App\Business\Data\Examining\ExaminationException
     */
    public function indexedAnswer(Request $request)
    {
        $index = $request->input('question_index');

        if ($index === 0) {
            usleep(500000);
        }

        $examinee = $this->examinees->one($request->input('uid'));
        $session = $this->sessions->last($examinee->uid);

        if (!$session) {
            throw new ExaminationException('No sessions found in ' . $examinee->uid);
        }

        if ($session->finished_at) {
            throw new ExaminationException('The last session is already finished');
        }

        // answer

        $answer = $this->answers->last($session->id);
        $string = $request->input('string');
        $has_ga = $request->has('ga');
        $ga = $request->input('ga');

        if ($ga === null) {
            $has_ga = false;
        }

        if ($index > 0 && !$answer) {
            throw new ExaminationException('Invalid index: (' . $index . ') \'cause the first answer expected');
        }

        if ($answer && $answer->index >= $index) {
            throw new ExaminationException(
                'Invalid index (' . $index . ') with the last (' . $answer->index . ') index'
            );
        }

        $request = new Request();
        $request->merge([
            'index' => $index,
            'answer' => $string,
            'session_id' => $session->id,
        ]);

        $answer = $this->answers->create($request);

        // GA

        if ($has_ga) {
            $examinee->ga = $ga;
            $examinee->save();
        }

        // result

        return [
            'ok' => true,
            'id' => $session->id,
            'answer' => $answer->id,
            'ga_added' => $has_ga,
        ];
    }
}
