<?php

namespace App\Models\Examining;

use App\Business\Data\Examining\ExamineExporter as ExamineExporterContract;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;

/**
 * Реализация экспортера результатов тестирования
 */
class ExamineExporter implements ExamineExporterContract
{
    /**
     * Возвращает хранилище выгрузок
     *
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    protected function getStatsStorage()
    {
        return Storage::drive('stats');
    }

    /**
     * Возвращает имя файла для выгрузки
     *
     * @param  string  $hash
     * @param  int  $mode
     * @return string
     */
    protected function getStatsName(string $hash, int $mode)
    {
        if ($mode < 0) {
            $ext = 'xls';
        } elseif ($mode > 0) {
            $ext = 'txt';
        } else {
            $ext = 'prc';
        }

        return $hash . '.' . $ext;
    }

    /**
     * Генерирует и возвращает имя файла для выгрузки
     *
     * @return string
     */
    public function generateStatsHash()
    {
        return md5(microtime());
    }

    /**
     * Возвращает true, если выгрузка готова
     *
     * @param  string  $hash
     * @return bool
     */
    public function areStatsReady(string $hash)
    {
        $storage = $this->getStatsStorage();

        $a = $storage->exists($this->getStatsName($hash, -1));
        $b = $storage->exists($this->getStatsName($hash, 1));
        $c = !$storage->exists($this->getStatsName($hash, 0));

        return ($a || $b) && $c;
    }

    /**
     * Возвращает ссылку на скачивание файла выгрузки
     *
     * @param  string  $hash
     * @return string
     */
    public function getStatsUrl(string $hash)
    {
        $storage = $this->getStatsStorage();
        $names = [
            $this->getStatsName($hash, -1),
            $this->getStatsName($hash, 1),
        ];

        foreach ($names as $name) {
            if ($storage->exists($name)) {
                return $storage->url($name);
            }
        }

        return '';
    }

    /**
     * Экспортирует статистику по тестированиям
     *
     * @param  string  $hash
     * @return void
     */
    public function exportStats(string $hash)
    {
        $storage = $this->getStatsStorage();

        $prc = $this->getStatsName($hash, 0);
        $storage->put($prc, '');

        // excel

        Excel::create(
            $hash,
            function (LaravelExcelWriter $excel) {
                $excel->sheet('stats', function (LaravelExcelWorksheet $sheet) {
                    $arr = [[
                        'uid',
                        'ga',
                        'sessid',
                        'q0',
                        'q1',
                        'q2',
                        'q3',
                        'q4',
                        'q5',
                        'q6',
                        'q7',
                        'q8',
                        'q9',
                    ]];

                    // models

                    $models = [
                        'examinees' => [],
                        'sessions' => [],
                        'answers' => [],
                    ];

                    foreach (array_keys($models) as $table) {
                        $pk = $table === 'examinees' ? 'uid' : 'id';

                        foreach (DB::table($table)->get() as $row) {
                            $models[$table][$row->$pk] = $row;
                        }
                    }

                    // data

                    $data = [];

                    foreach ($models['answers'] as $answer) {
                        $session = $models['sessions'][$answer->session_id];
                        $examinee = $models['examinees'][$session->examinee_uid];

                        $key = $examinee->uid . '--' . $session->id;

                        if (!array_key_exists($key, $data)) {
                            $data[$key] = [
                                'uid' => $examinee->uid,
                                'ga' => $examinee->ga,
                                'sessid' => $session->id,
                            ];

                            for ($i = 0; $i < 10; $i++) {
                                $data[$key]['q' . $i] = '';
                            }
                        }

                        $data[$key]['q' . $answer->index] = $answer->answer;
                    }

                    // rows

                    foreach ($data as $row) {
                        $arr[] = $row;
                    }

                    $sheet->fromArray($arr, null, 'A1', false, false);
                });
            }
        )->store('xls', $storage->path(''));

        // end of excel

        $storage->delete($prc);
    }

    /**
     * Оповещает об ошибке во время экспорта
     *
     * @param  string  $hash
     * @param  \Exception  $ex
     * @return void
     */
    public function erroneousStats(string $hash, Exception $ex)
    {
        $storage = $this->getStatsStorage();
        $names = [
            $this->getStatsName($hash, -1),
            $this->getStatsName($hash, 0),
        ];

        foreach ($names as $name) {
            if ($storage->exists($name)) {
                $storage->delete($name);
            }
        }

        $storage->put($this->getStatsName($hash, 1), $ex->getMessage());
    }
}
