<?php

namespace App\Models\Repositories;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as RequestFacade;

/**
 * Репозиторий испытуемых
 */
class ExamineeRepository extends Repository
{
    /**
     * Возвращает испытуемого на основе уникального идентификатора, взятого из кукисов, либо сгенерированный
     *
     * @return \App\Models\Examinee|\Illuminate\Database\Eloquent\Model
     */
    public function determine()
    {
        // define

        $uid = $this->cookieUid();

        if ($uid === null) {
            $uid = $this->generateUid();
        }

        // model

        try {
            $examinee = $this->one($uid);
        } catch (Exception $ex) {
            $request = new Request();
            $request->merge([
                'uid' => $uid,
                'data' => $this->visitorData(),
                'ga' => '',
            ]);

            $examinee = $this->create($request);
        }

        // result

        $this->cookieUid($uid);
        return $examinee;
    }

    /**
     * Возвращает, либо задает уникальный идентификатор в кукисы
     *
     * @param  $uid
     * @return string
     */
    public function cookieUid($uid = null)
    {
        $key = 'TUSH18UID';

        // getter

        if ($uid === null) {
            return Cookie::get($key);
        }

        // setter

        /** @var  \Illuminate\Cookie\CookieJar  $cookie */
        $cookie = Cookie::getFacadeRoot();

        $cookie->queue($cookie->forever($key, $uid));
        return $uid;
    }

    /**
     * Возвращает данные посетителя
     *
     * @return array
     */
    public function visitorData()
    {
        /** @var  \Illuminate\Http\Request  $request */
        $request = RequestFacade::getFacadeRoot();

        return [
            'ip' => $request->server('REMOTE_ADDR'),
            'stamp' => time(),
        ];
    }

    /**
     * Генерирует уникальный идентификатор
     *
     * @return string
     */
    public function generateUid()
    {
        $data = $this->visitorData();

        return sha1(implode('!', [
            $data['ip'],
            $data['stamp'],
        ]));
    }

    /**
     * Возвращает некоторую статистику по испытуемым
     *
     * @return array
     */
    public function stats()
    {
        $a = $this->startQuery()->count();

        /** @var  \Illuminate\Database\Query\Builder  $q */
        $q = DB::table('examinees');
        $q->leftJoin('sessions', 'examinees.uid', '=', 'sessions.examinee_uid');

        $uids = (clone $q)
            ->select(DB::raw('DISTINCT(examinees.uid) as uid'))
            ->whereNotNull('sessions.finished_at')
            ->pluck('uid')
            ->toArray();

        $b = count($uids);
        $c = (clone $q)
            ->select(DB::raw('COUNT(DISTINCT(examinees.uid)) as C'))
            ->whereNull('sessions.finished_at')
            ->whereNotNull('sessions.id')
            ->whereNotIn('sessions.examinee_uid', $uids)
            ->value('C');

        return [
            'people' => $a,
            'passed' => $b,
            'began' => $c,
            'unbegan' => $a - $b - $c,
        ];
    }
}
