<?php

namespace App\Models\Repositories;

use Illuminate\Http\Request;

/**
 * Репозиторий тестовых сессий
 */
class SessionRepository extends Repository
{
    /**
     * Стартует запрос для выборки всех моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function queryAll(Request $request = null)
    {
        $q = parent::queryAll($request);

        if ($request && $request->has('uid')) {
            $q->where('examinee_uid', $request->input('uid'));
        }

        return $q;
    }

    /**
     * Возвращает запись последней добавленной сессии
     *
     * @param  string  $examinee_uid
     * @return \Illuminate\Database\Eloquent\Model|\App\Models\Session|null
     */
    public function last(string $examinee_uid)
    {
        return $this->startQuery()
            ->where('examinee_uid', $examinee_uid)
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * Возвращает некоторую статистику по сессиям тестирований
     *
     * @return array
     */
    public function stats()
    {
        return [
            'all' => $this->startQuery()->count(),
            'finished' => $this->startQuery()->whereNotNull('finished_at')->count(),
            'unfinished' => $this->startQuery()->whereNull('finished_at')->count(),

            'sex' => $this->startQuery()->whereNotNull('sex')->count(),
            'unsex' => $this->startQuery()->whereNull('sex')->count(),
            'male' => $this->startQuery()->where('sex', 'm')->count(),
            'female' => $this->startQuery()->where('sex', 'f')->count(),

            'age' => $this->startQuery()->whereNotNull('age')->count(),
            'unage' => $this->startQuery()->whereNull('age')->count(),
            'lt30' => $this->startQuery()->where('age', '_30')->count(),
            '31to45' => $this->startQuery()->where('age', '31-45')->count(),
            '46to60' => $this->startQuery()->where('age', '46-60')->count(),
            'gt60' => $this->startQuery()->where('age', '60_')->count(),
        ];
    }
}
