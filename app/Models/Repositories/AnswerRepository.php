<?php

namespace App\Models\Repositories;

/**
 * Репозиторий ответов
 */
class AnswerRepository extends Repository
{
    /**
     * Возвращает запись последнего добавленного ответа
     *
     * @param  int  $session_id
     * @return \Illuminate\Database\Eloquent\Model|\App\Models\Answer|null
     */
    public function last(int $session_id)
    {
        return $this->startQuery()
            ->where('session_id', $session_id)
            ->orderBy('index', 'desc')
            ->first();
    }
}
