<?php

namespace App\Jobs;

use App\Business\Data\Examining\ExamineExporter;
use Exception;

class ExportStats extends Job
{
    /**
     * Хеш-имя файла выгрузки
     *
     * @var string
     */
    public $hash;

    /**
     * Экспортер результатов тестирования
     *
     * @var \App\Business\Data\Examining\ExamineExporter
     */
    protected $exporter;

    /**
     * Создает экземпляр джоба
     *
     * @param  string  $hash
     */
    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * Инициализации перед выполнением джоба
     *
     * @return void
     */
    protected function startup()
    {
        parent::startup();

        $this->exporter = app()->make(ExamineExporter::class);
    }

    /**
     * Выполняет джоб, до того, как происходит логика завершения выполнения
     *
     * @return void
     */
    protected function perform()
    {
        $this->exporter->exportStats($this->hash);
    }

    /**
     * Действия перед основной обработкой ошибок
     *
     * @param  \Exception  $ex
     * @return void
     */
    protected function preFail(Exception $ex)
    {
        $this->exporter->erroneousStats($this->hash, $ex);
    }
}
