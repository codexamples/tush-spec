<?php

namespace App\Exceptions;

use App\Business\Data\Examining\ExaminationException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use ReflectionClass;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->is('api/*')) {
            $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        }

        if ($exception instanceof ExaminationException) {
            $exception = new HttpException(400, $exception->getMessage(), $exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert the given exception to an array.
     *
     * @param  \Exception  $e
     * @return array
     */
    protected function convertExceptionToArray(Exception $e)
    {
        if ($this->isHttpException($e) && $e->getPrevious() !== null) {
            $e = $e->getPrevious();
        }

        if (config('app.debug')) {
            return parent::convertExceptionToArray($e);
        }

        $reflection = new ReflectionClass(get_class($e));
        $arr = ['exception' => $reflection->getShortName()];

        if ($e->getMessage()) {
            $arr['message'] = $e->getMessage();
        }

        return $arr;
    }
}
