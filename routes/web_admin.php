<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'IndexController@index')->name('admin');

Route::group(['prefix' => 'password'], function () {
    Route::get('/', 'PasswordController@form')->name('admin.password');
    Route::post('/save', 'PasswordController@save')->name('admin.password.save');
});

/*
|--------------------------------------------------------------------------
| Models
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'examinee'], function () {
    Route::get('/', 'ExamineeController@index')->name('admin.examinee');
    Route::get('/{uid}', 'ExamineeController@view')->name('admin.examinee.view');
});

Route::group(['prefix' => 'session'], function () {
    Route::get('/', 'SessionController@index')->name('admin.session');
    Route::get('/{id}', 'SessionController@view')->name('admin.session.view');
});

Route::get('/stats', 'StatsController@index')->name('admin.stats');
