<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::group([
    'middleware' => 'guest',
    'prefix' => 'login'
], function () {
    Route::get('/', 'LoginController@showLoginForm')->name('login');
    Route::post('/', 'LoginController@login');
});

Route::post('/logout', 'LoginController@logout')
    ->middleware('auth')
    ->name('logout');

/*
|--------------------------------------------------------------------------
| Special
|--------------------------------------------------------------------------
|
*/

Route::get('/result-{session_id}', 'ResultController@index')->name('result');

/*
|--------------------------------------------------------------------------
| Web Routes Continue
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'IndexController@index');
