<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'answer'], function () {
    Route::post('/personal', 'AnswerController@personal');
    Route::post('/question', 'AnswerController@question');
    Route::post('/finish', 'AnswerController@finish');
    Route::get('/{question_index}', 'AnswerController@indexed');
});

Route::group(['prefix' => 'stats-export'], function () {
    Route::post('/', 'StatsExportController@start');
    Route::get('/{hash}', 'StatsExportController@status');
});
