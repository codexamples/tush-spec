<?php /** @var  \App\Business\Data\Examining\ExamineResultAccessible  $session */ ?>
<h1>General</h1>
<img src="{{ $session->result_image }}">
<h1>Ok</h1>
<img src="{{ $session->result_image_ok }}">
@php($txts = $session->getExamineResultTexts())
<h1>Text A</h1>
<b>{{ $txts['a']['title'] }}</b>
<p>{{ $txts['a']['text'] }}</p>
<h1>Text B</h1>
<p>{{ $txts['b'] }}</p>
