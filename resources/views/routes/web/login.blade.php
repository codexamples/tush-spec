@extends('layouts.auth')

@section('content')
    <form class="form-horizontal m-t-20" method="post" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <div class="col-xs-12">
                <input
                        class="form-control"
                        type="text"
                        placeholder="Логин"
                        name="name"
                        value="{{ old('name') }}"
                        required
                >
                <i class="md md-account-circle form-control-feedback l-h-34"></i>
                @if($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="col-xs-12">
                <input
                        class="form-control"
                        type="password"
                        placeholder="Пароль"
                        name="password"
                        value=""
                        required
                >
                <i class="md md-vpn-key form-control-feedback l-h-34"></i>
                @if($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary">
                    <input
                            id="checkbox-signup"
                            type="checkbox"
                            name="remember"
                            @if(old('remember')) checked @endif
                    >
                    <label for="checkbox-signup">
                        Запомнить меня
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group text-right m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">
                    Войти
                </button>
            </div>
        </div>
    </form>
@endsection
