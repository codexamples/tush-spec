<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <meta property="og:image" content="https://***.ru/tush-meta.jpg">
        <meta property="og:image:width" content="968">
        <meta property="og:image:height" content="504">
        <title>ТушСпец</title>
        <script type="text/javascript">
            window.visitorUid = '{{ $uid }}';
        </script>
        <link href="{{ asset('css/index.css?' . $hash['css']) }}" rel="stylesheet">

        @if(!config('app.debug'))
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){/* gtm */})(window,document,'script','dataLayer','GTM-***');</script>
            <!-- End Google Tag Manager -->

        @endif

    </head>
    <body>
        @if(!config('app.debug'))
          <!-- Google Tag Manager (noscript) -->
          <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-***"
          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
          <!-- End Google Tag Manager (noscript) -->
        @endif

        <div id="app"></div>
        <script type="text/javascript" src="{{ asset('bundle.js?' . $hash['js']) }}"></script>
    </body>
</html>
