<?php /** @var  \App\Models\Session  $session */ ?>
@php($title = 'Калькулятор посчитает, насколько здоровый образ жизни ты ведешь')
@php($result = $session->getExamineResult())
@php($img_field = $target ? 'result_image_' . $target : 'result_image')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $title }}</title>

        <meta property="og:url" content="{{ route('result', $session->id) }}">
        <meta property="og:title" content="{{ $title }}">
        <meta property="og:description" content="У меня {{ $result }} час{{ word_end_choice($result, '', 'а', 'ов') }} здорового времени!">
        <meta property="og:image" content="{{ $session->$img_field }}">
    </head>
    <body>
        <img src="{{ $session->$img_field }}" style="opacity: 0;">
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function() {
              setTimeout(function() {
                document.write('Redirecting...');

                setTimeout(function() {
                  location.href = '/';
                }, 500);
              }, 500);
            });
        </script>
    </body>
</html>
