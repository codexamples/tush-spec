@extends('layouts.admin')

@section('content')
    @php($box_style = 'border: 0; background: transparent;')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box" style="{{ $box_style }}">
                <h4 class="header-title text-dark">
                    Количество испытуемых
                </h4>
                <h1 class="m-t-30 text-muted stats-count pull-left">{{ number_format($stats['people'], 0, ',', ' ') }}</h1>
                <button class="btn btn-primary m-t-30 pull-right" id="export-stats">
                    <i class="fa fa-table"></i> <span>Выгрузить</span>
                </button>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="card-box">
                @php($p = $stats['people'] === 0 ? 0 : $stats['passed'] / $stats['people'] * 100)
                <h4 class="header-title text-dark">
                    Прошли тестирование
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['passed'], 0, ',', ' ') }}</p>
                <div class="chart chrt-passed pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card-box">
                @php($p = $stats['people'] === 0 ? 0 : $stats['began'] / $stats['people'] * 100)
                <h4 class="header-title text-dark">
                    Начали, но не прошли тестирование
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['began'], 0, ',', ' ') }}</p>
                <div class="chart chrt-began pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card-box">
                @php($p = $stats['people'] === 0 ? 0 : $stats['unbegan'] / $stats['people'] * 100)
                <h4 class="header-title text-dark">
                    Не начинали тестирование
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['unbegan'], 0, ',', ' ') }}</p>
                <div class="chart chrt-unbegan pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box" style="{{ $box_style }}">
                <h4 class="header-title text-dark">
                    Количество сессий
                </h4>
                <h1 class="m-t-30 text-muted stats-count">{{ number_format($stats['all'], 0, ',', ' ') }}</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['finished'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Завершенные
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['finished'], 0, ',', ' ') }}</p>
                <div class="chart chrt-finished pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['unfinished'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Незавершенные
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['unfinished'], 0, ',', ' ') }}</p>
                <div class="chart chrt-unfinished pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box" style="{{ $box_style }}">
                <h4 class="header-title text-dark">
                    Пол
                </h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['sex'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Пол указан
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['sex'], 0, ',', ' ') }}</p>
                <div class="chart chrt-sex pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['unsex'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Пол не указан
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['unsex'], 0, ',', ' ') }}</p>
                <div class="chart chrt-unsex pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['male'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Мужской
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['male'], 0, ',', ' ') }}</p>
                <div class="chart chrt-male pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['female'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Женский
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['female'], 0, ',', ' ') }}</p>
                <div class="chart chrt-female pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box" style="{{ $box_style }}">
                <h4 class="header-title text-dark">
                    Возраст
                </h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['age'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Возраст указан
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['age'], 0, ',', ' ') }}</p>
                <div class="chart chrt-age pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['unage'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    Возраст не указан
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['unage'], 0, ',', ' ') }}</p>
                <div class="chart chrt-unage pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['lt30'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    До 30 лет
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['lt30'], 0, ',', ' ') }}</p>
                <div class="chart chrt-_30 pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['31to45'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    От 31 до 45 лет
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['31to45'], 0, ',', ' ') }}</p>
                <div class="chart chrt-31_45 pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['46to60'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    От 46 до 60 лет
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['46to60'], 0, ',', ' ') }}</p>
                <div class="chart chrt-46_60 pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card-box">
                @php($p = $stats['all'] === 0 ? 0 : $stats['gt60'] / $stats['all'] * 100)
                <h4 class="header-title text-dark">
                    От 60 лет
                </h4>
                <p class="text-muted h2 stats-count">{{ number_format($stats['gt60'], 0, ',', ' ') }}</p>
                <div class="chart chrt-60_ pull-right" data-percent="{{ $p }}">
                    <span class="percent"></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    @parent
    <style>
        .stats-count {
            display: none;
        }
    </style>
@endsection

@section('js_bottom')
    @parent
    <script src="{{ asset('assets/plugins/jquery.easy-pie-chart/dist/easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <script>
        @php($charts = [
            'passed',
            'began',
            'unbegan',

            'finished',
            'unfinished',

            'sex',
            'unsex',
            'male',
            'female',

            'age',
            'unage',
            '_30',
            '31_45',
            '46_60',
            '60_',
        ])
        $(document).ready(function() {
          // charts

            @foreach($charts as $key)
                $('.chrt-{{ $key }}').easyPieChart({
                    easing: 'easeOutBounce',
                    barColor : 'rgb({{ rand(0, 200) }}, {{ rand(0, 200) }}, {{ rand(0, 200) }})',
                    lineWidth: 10,
                    trackColor : false,
                    lineCap : 'butt',
                    onStep: function(from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    }
                });
            @endforeach

          // counts

          $('.stats-count').fadeIn('slow');

          // export stats

          var exportId = '#export-stats';

          $(exportId).on('click', function() {
            var turnOff = function(off) {
              $(exportId)
                .prop('disabled', off)
                .find('span')
                .html(off ? 'Выгружаю...' : 'Выгрузить');
            };

            var onError = function(xhr, type, msg) {
              turnOff(false);
              alert(msg);
            };

            turnOff(true);

            $.ajax({
              url: '/api/stats-export',
              type: 'post',
              error: onError,
              success: function(data) {
                var hash = data.toString();
                var status = function() {
                  $.ajax({
                    url: '/api/stats-export/' + hash,
                    type: 'get',
                    error: onError,
                    success: function(data) {
                      var url = data.toString();

                      if (url.length > 0) {
                        turnOff(false);
                        location.href = url;
                      } else {
                        setTimeout(status, 250);
                      }
                    }
                  });
                };

                status();
              }
            });
          });
        });
    </script>
@endsection
