@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Models\Session  $session */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                @if(!$session->finished_at)
                    <div class="alert alert-warning">
                        Сессия не была завершена
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="id">№</label>
                        <input
                                id="id"
                                type="text"
                                class="form-control"
                                value="{{ $session->id }}"
                                readonly
                        >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="sex">Пол</label>
                        <input
                                id="sex"
                                type="text"
                                class="form-control"
                                value="{{ $session->human_sex }}"
                                readonly
                        >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="age">Возраст</label>
                        <input
                                id="age"
                                type="text"
                                class="form-control"
                                value="{{ $session->human_age }}"
                                readonly
                        >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="answers">Отвечено</label>
                        <input
                                id="answers"
                                type="text"
                                class="form-control"
                                value="{{ $session->answers }}"
                                readonly
                        >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="score">Баллы</label>
                        <input
                                id="score"
                                type="text"
                                class="form-control"
                                value="{{ $session->score }}"
                                readonly
                        >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="finished_at">Начато</label>
                        <input
                                id="finished_at"
                                type="text"
                                class="form-control"
                                value="{{ $session->created_at }}"
                                readonly
                        >
                    </div>
                </div>
                @if($session->finished_at)
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="finished_at">Завершено</label>
                            <input
                                    id="finished_at"
                                    type="text"
                                    class="form-control"
                                    value="{{ $session->finished_at }}"
                                    readonly
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Результат</label>
                            <div class="box-cont ann m-b-10">
                                <img src="{{ $session->result_image }}">
                            </div>
                            <div class="box-cont ann m-b-10">
                                <img src="{{ $session->result_image_ok }}">
                            </div>
                            @php($texts = $session->getExamineResultTexts())
                            <textarea
                                    class="form-control m-b-10"
                                    readonly
                            >{!! $texts['a']['title'] . "\n\n" . $texts['a']['text'] !!}</textarea>
                            <textarea
                                    class="form-control"
                                    readonly
                            >{!! $texts['b'] !!}</textarea>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Испытуемый</label>
                        <div class="form-control">
                            <a href="{{ route('admin.examinee.view', $session->examinee->uid) }}">
                                {{ $session->examinee->short_uid }}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
