@extends('layouts.admin')

@section('content')
    <?php /** @var  \Illuminate\Pagination\LengthAwarePaginator  $sessions */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Пол</th>
                        <th>Возраст</th>
                        <th>Отвечено</th>
                        <th>Баллы</th>
                        <th>Время</th>
                        <th>Превью</th>
                        <th>Испытуемый</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($sessions as $session)
                        <?php /** @var  \App\Models\Session  $session */ ?>
                            <tr>
                                <td>{{ $session->id }}</td>
                                <td>{{ $session->human_sex }}</td>
                                <td>{{ $session->human_age }}</td>
                                <td>{{ $session->answers }}</td>
                                <td>{{ $session->score }}</td>
                                <td>
                                    {{ $session->created_at }}
                                    @if($session->finished_at)
                                        <br>{{ $session->finished_at }}
                                    @endif
                                </td>
                                <td>
                                    @if($session->finished_at)
                                        <div class="box-cont ann">
                                            <img src="{{ $session->result_image }}">
                                        </div>
                                    @else
                                        <i>Нет</i>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-link" href="{{ route('admin.examinee.view', $session->examinee->uid) }}">
                                        {{ $session->examinee->short_uid }}
                                        <i class="fa fa-external-link"></i>
                                    </a>
                                </td>
                                <td>
                                    <a
                                            href="{{ route('admin.session.view', $session->id) }}"
                                            class="btn btn-inverse"
                                            title="Просмотр"
                                    >
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr><td colspan="9" class="text-muted text-center">Пусто</td></tr>
                        @endforelse
                    </tbody>
                </table>
                {!! $sessions->links() !!}
            </div>
        </div>
    </div>
@endsection
