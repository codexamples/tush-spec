@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Models\Examinee  $examinee */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                @if($examinee->uid === $cookie_uid)
                    <div class="alert alert-info">
                        Это Вы
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="uid">UID</label>
                        <input
                                id="uid"
                                type="text"
                                class="form-control"
                                value="{{ $examinee->uid }}"
                                readonly
                        >
                    </div>
                </div>
                @if($examinee->ga)
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="gas">GA</label>
                            <input
                                    id="ga"
                                    type="text"
                                    class="form-control"
                                    value="{{ $examinee->ga }}"
                                    readonly
                            >
                        </div>
                    </div>
                @endif
                @foreach($examinee->data as $k => $v)
                    <?php $examinee->mutateDataField($k, $v); ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="uid">{{ $k }}</label>
                            <input
                                    id="uid"
                                    type="text"
                                    class="form-control"
                                    value="{{ $v }}"
                                    readonly
                            >
                        </div>
                    </div>
                @endforeach
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Объекты</label>
                        <div class="form-control">
                            <a href="{{ route('admin.session', ['uid' => $examinee->uid]) }}">
                                Посмотреть все
                                ({{ $examinee->sessions()->count() }})
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
