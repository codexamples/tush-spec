@extends('layouts.admin')

@section('content')
    <?php /** @var  \Illuminate\Pagination\LengthAwarePaginator  $examinees */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>UID</th>
                        <th>GA</th>
                        <th>Данные</th>
                        <th>Сессии</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($examinees as $examinee)
                        <?php /** @var  \App\Models\Examinee  $examinee */ ?>
                            <tr{!! $examinee->uid === $cookie_uid ? ' class="info"' : '' !!}>
                                <td title="{{ $examinee->uid }}">
                                    {{ $examinee->short_uid }}
                                    @if($examinee->uid === $cookie_uid)
                                        <sup><i class="text-muted">Это Вы</i></sup>
                                    @endif
                                </td>
                                <td>
                                    @if($examinee->ga)
                                        {{ $examinee->ga }}
                                    @else
                                        <i>Нет</i>
                                    @endif
                                </td>
                                <td>
                                    @forelse($examinee->data as $k => $v)
                                        <?php $examinee->mutateDataField($k, $v); ?>
                                        @if(!$loop->first)
                                            <br><br>
                                        @endif
                                        <b>{{ $k }}:</b>
                                        <br>{{ $v }}
                                    @empty
                                        <i>Нет</i>
                                    @endforelse
                                </td>
                                <td>
                                    <a class="btn btn-link" href="{{ route('admin.session', ['uid' => $examinee->uid]) }}">
                                        {{ $examinee->sessions()->count() }}
                                        <i class="fa fa-external-link"></i>
                                    </a>
                                </td>
                                <td>
                                    <a
                                            href="{{ route('admin.examinee.view', $examinee->uid) }}"
                                            class="btn btn-inverse"
                                            title="Просмотр"
                                    >
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr><td colspan="4" class="text-muted text-center">Пусто</td></tr>
                        @endforelse
                    </tbody>
                </table>
                {!! $examinees->links() !!}
            </div>
        </div>
    </div>
@endsection
