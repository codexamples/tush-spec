<ul>
    <?php
        /** @var  \App\Business\Support\Navigation\CurrentMenu  $current_menu */
    ?>
    <li class="text-muted menu-title">Тестирование</li>
    <li>
        <a href="{{ route('admin.examinee') }}" class="waves-effect waves-primary @if($current_menu->inLeft('examinee', 1)) subdrop @endif">
            <i class="md md-android"></i>
            <span>Испытуемые</span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin.session') }}" class="waves-effect waves-primary @if($current_menu->inLeft('session', 1)) subdrop @endif">
            <i class="md md-grain"></i>
            <span>Сессии</span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin.stats') }}" class="waves-effect waves-primary @if($current_menu->inLeft('stats', 1)) subdrop @endif">
            <i class="md md-insert-chart"></i>
            <span>Статистика</span>
        </a>
    </li>
</ul>
