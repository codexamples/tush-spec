@extends('html')

@section('title', 'Not Found')

@section('css')
    <style>
        .bg {
            background-image: url('{{ asset('assets/bg.png') }}');
            background-size: cover;
            background-repeat: no-repeat;
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            z-index: 0;
        }

        .logo {
            position: absolute;
            left: 50%;
            top: 5%;
            transform: translateX(-50%);
            z-index: 2;
        }

        .main {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translateX(-50%);
            color: white;
            text-transform: uppercase;
            letter-spacing: 1px;
            z-index: 1;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        .main h1 {
            font-size: 32px;
            font-weight: 400;
        }

        .main p {
            font-size: 14px;
            font-weight: 500;
            text-align: center;
        }

        .main .btn {
            text-align: center;
            margin-top: 50px;
        }

        .main .btn a {
            display: inline-block;
            padding: 15px 30px;
            border-radius: 25px;
            background: linear-gradient(90deg, #f69c44, #f55455);
            color: white;
            text-decoration: none;
            font-size: 16px;
        }
    </style>
@endsection

@section('body_content')
    <div class="bg">
        <img src="{{ asset('assets/logo.png') }}" class="logo">
        <div class="main">
            <h1>Кто-то похитил вашу страницу</h1>
            <p>Ошибка 404</p>
            <div class="btn">
                <a href="/">Подсчитать здоровое время</a>
            </div>
        </div>
    </div>
@endsection
