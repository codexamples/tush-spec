jobbed = {
  showDots: true,
  dotCount: 0,
  existingKeys: []
};

$(document).ready(function() {
  $('#jobbed-done').on('hidden.bs.modal', function() {
    location.href = '/admin';
  });

  jobbed.dots();
  jobbed.requestData();
});

jobbed.dots = function() {
  if (jobbed.showDots) {
    if (jobbed.dotCount >= 5) {
      jobbed.dotCount = 0;
    } else {
      jobbed.dotCount++;
    }

    var arr = [];

    for (var i = 0; i < jobbed.dotCount; i++) {
      arr.push('.');
    }

    $('#jobbed-dots').html(arr.join(''));
  }

  setTimeout(jobbed.dots, 250);
};

jobbed.requestData = function() {
  $.ajax({
    url: '/api/jobbed',
    error: jobbed.ajaxError,
    success: jobbed.ajaxResponse
  });
};

jobbed.ajaxError = function(xhr, type, msg) {
  jobbed.showDots = false;

  $('#jobbed-dots').html('<hr><small class="text-danger">' + msg + '</small><hr>');

  jobbed.repeatRequest();
};

jobbed.ajaxResponse = function(data) {
  var repeat = true;

  if (typeof(data) === 'object') {
    jobbed.showDots = true;

    $('#jobbed-type').html(data.type);

    if (data.status) {
      repeat = false;

      $('#jobbed-done--body').html(data.status);
      $('#jobbed-done').modal('show');
    } else if (data.data) {
      Object.keys(data.data).map(function(key) {
        if (!jobbed.existingKeys.hasOwnProperty(key)) {
          jobbed.existingKeys[key] = $('<tr></tr>').html('<td class="jk"></td><td><i class="jv"></i></td>');

          $('#jobbed-tbl').append(jobbed.existingKeys[key]);
        }

        jobbed.existingKeys[key].find('.jk').html(key);
        jobbed.existingKeys[key].find('.jv').html(data.data[key]);
      });
    }
  } else {
    jobbed.showDots = false;

    $('#jobbed-dots').html('<hr><small class="text-danger">' + msg + '</small><hr>');
  }

  if (repeat) {
      jobbed.repeatRequest();
  }
};

jobbed.repeatRequest = function() {
  setTimeout(jobbed.requestData, 400);
};
