document.addEventListener('DOMContentLoaded', function() {
    var clipboard = new Clipboard('.clipboard-btn');

    clipboard.on('success', function(e) {
        alert('Скопировано:\n' + e.text);
    });

    clipboard.on('error', function() {
        alert('Не удалось скопировать');
    });
});
