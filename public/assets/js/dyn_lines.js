dynLines = {
    idPrefix: null
};

dynLines.start = function(lineData, idPrefix) {
    dynLines.idPrefix = idPrefix;

    if (lineData === null) {
        dynLines.appendField('');
    } else {
        lineData.map(function(line) {
            dynLines.appendField(line);
        });
    }

    $('#' + dynLines.idPrefix + 'add').on('click', function() {
        dynLines.appendField('');
        return false;
    });
};

dynLines.appendField = function(line) {
    var colMd12 = $('<div></div>')
        .addClass('col-md-12')
        .addClass('m-t-10');
    $('#' + dynLines.idPrefix + 'lines').append(colMd12);

    var valueColMd5 = $('<div></div>').addClass('col-md-10');
    colMd12.append(valueColMd5);

    var delColMd2 = $('<div></div>').addClass('col-md-2');
    colMd12.append(delColMd2);

    var lineField = $('<input />')
        .addClass('form-control')
        .prop('type', 'text')
        .prop('name', 'lines[]')
        .val(line);
    valueColMd5.append(lineField);

    var delButton = $('<button></button>')
        .addClass('btn')
        .addClass('btn-block')
        .addClass('btn-custom')
        .addClass('btn-danger')
        .prop('title', 'Удалить строку')
        .html('<i class="fa fa-trash"></i>')
        .on('click', function() {
            colMd12.remove();
            return false;
        });
    delColMd2.append(delButton);
};
