uploadWnd = {};

uploadWnd.start = function(selectors, uri) {
  selectors.map(function(selector) {
    var el = $(selector);

    $('*[data-clear="' + selector + '"]').on('click', function() {
      $($(this).data('clear')).val('');
    });

    el.prop('type', 'text').prop('readonly', true).on('click', function() {
      var wnd = window.open(uri, 'wndLargeUpload', 'width=640,height=480');

      wnd.fld = $(this);

      wnd.closeHandler = function(values) {
        wnd.fld.val(values.length === 0 ? '' : values[0]);
      };

      wnd.doClose = function(values) {
        wnd.closeHandler(values);
        wnd.close();
      };
    });
  });
};
