dynFields = {
    idPrefix: null
};

dynFields.start = function(fieldData, idPrefix) {
    dynFields.idPrefix = idPrefix;

    if (fieldData === null) {
        dynFields.appendField('', '');
    } else {
        for (var key in fieldData) {
            dynFields.appendField(key, fieldData[key]);
        }
    }

    $('#' + dynFields.idPrefix + 'add').on('click', function() {
        dynFields.appendField('', '');
        return false;
    });
};

dynFields.appendField = function(key, value) {
    var colMd12 = $('<div></div>')
        .addClass('col-md-12')
        .addClass('m-t-10');
    $('#' + dynFields.idPrefix + 'fields').append(colMd12);

    var keyColMd5 = $('<div></div>').addClass('col-md-5');
    colMd12.append(keyColMd5);

    var valueColMd5 = $('<div></div>').addClass('col-md-5');
    colMd12.append(valueColMd5);

    var delColMd2 = $('<div></div>').addClass('col-md-2');
    colMd12.append(delColMd2);

    var keyField = $('<input />')
        .addClass('form-control')
        .prop('type', 'text')
        .prop('name', 'keys[]')
        .val(key);
    keyColMd5.append(keyField);

    var valueField = $('<input />')
        .addClass('form-control')
        .prop('type', 'text')
        .prop('name', 'values[]')
        .val(value);
    valueColMd5.append(valueField);

    var delButton = $('<button></button>')
        .addClass('btn')
        .addClass('btn-block')
        .addClass('btn-custom')
        .addClass('btn-danger')
        .prop('title', 'Удалить поле')
        .html('<i class="fa fa-trash"></i>')
        .on('click', function() {
            colMd12.remove();
            return false;
        });
    delColMd2.append(delButton);
};
